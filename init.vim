" -- general flags

syntax      on

set nowrap "prevent wrapping of text"
set number "show numbers"
set splitbelow "create a new split below"
set splitright "create a vertical split always on right"
set nocompatible "remove vi compatibility"
set autochdir
set encoding=utf-8
set scrolloff=3
set autoindent
set smartindent " Use confirmation dialog.
set confirm " Don't use annoying sounds.
set visualbell " Remember lots of commands.
set history=10000 " Show matching braces but not for too long.
set incsearch
set hlsearch
set ignorecase
set smartcase " Enable using the mouse like some everyday guy.
set mouse=a " Show interesting stuff at the bottom of the window.
set showcmd
set ruler
set iskeyword+=-
set wildmenu
set wildmode=list:longest
set wildignore+=.DS_Store,Thumbs.db
set wildignore+=*.so,*.dll,*.exe,*.lib,*.pdb
set wildignore+=*.pyc,*.pyo
set wildignore+=*.swp"
set ttyfast
set backspace=indent,eol,start
set whichwrap+=<,>,h,l
set complete=.,w,b,u,t
set clipboard=unnamedplus
set noequalalways
set omnifunc=syntaxcomplete#Complete
set hidden
" -- fold settings
set foldmethod=indent
set foldcolumn=0
set nofoldenable
" -- tab settings
set tabstop=4
set shiftwidth=4
set softtabstop=4
set noexpandtab
" -- dir settings
set backupdir=~/.backup/
set directory=~/.swp/
set undodir=~/.undo/
" -- ag for grep
set grepprg=ag

"  Plug Start 
call plug#begin('~/.config/nvim/plugged')

" -- Theming 
Plug 'sainnhe/gruvbox-material'
Plug 'ajmwagar/vim-deus'
Plug 'nvim-lualine/lualine.nvim'
Plug 'kdheepak/tabline.nvim'
Plug 'Everblush/everblush.nvim', { 'as': 'everblush' }

" -- commenting
Plug 'scrooloose/nerdcommenter'

" -- pairing 
Plug 'tpope/vim-surround'
Plug 'windwp/nvim-autopairs'
Plug 'tmhedberg/matchit'

" -- git 
Plug 'tpope/vim-fugitive'

" -- file management
Plug 'nvim-tree/nvim-web-devicons' 
Plug 'nvim-tree/nvim-tree.lua'

" -- autoformat -
Plug 'Chiel92/vim-autoformat'

" -- Frontend utilities 
Plug 'mattn/emmet-vim',{'for':['html','htmldjango','javascript','typescriptreact']}
Plug 'leafgarland/typescript-vim',{'for':'typescript'}
Plug 'ap/vim-css-color'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}

" -- Markdown/Writing Utilities -
Plug 'plasticboy/vim-markdown',{'for':['markdown,vimwiki']}
Plug 'skywind3000/asyncrun.vim' "for octodown preview
Plug 'lervag/vimtex'

" -- tasks Utilities -
Plug 'kvrohit/tasks.nvim'

" -- Python Utilities -
Plug 'lukas-reineke/indent-blankline.nvim'
"Plug 'fisadev/vim-isort'
"Plug 'mgedmin/python-imports.vim'

" -- Wiki Utilities -
Plug 'vimwiki/vimwiki'
Plug 'MattesGroeger/vim-bookmarks'

" -- Ultisnips -
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'quangnguyen30192/cmp-nvim-ultisnips'

" -- search and highlighting  -
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'

"- vim session 
Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'
Plug 'webdevel/tabulous' " tab management

Plug 'airblade/vim-rooter'

Plug 'folke/which-key.nvim'
Plug 'folke/todo-comments.nvim'
Plug 'christoomey/vim-tmux-navigator'

call plug#end()


filetype    plugin indent on
" -- color settings 
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors
set background=dark
"let g:gruvbox_material_background = 'hard'
"let g:gruvbox_material_better_performance = 1
"let g:gruvbox_material_enable_italic = 1
"colorscheme gruvbox-material
"highlight Comment cterm=italic

"- python host config (autoformat) --
let g:python3_host_prog="/usr/bin/python3"

let g:formatters_python = ['black']
let g:formatters_typescript = ['eslint_local','prettier']
let g:formatters_scss = ['stylelint_local','prettier']
let g:run_all_formatters_typescript = 1
let g:run_all_formatters_python = 1

let mapleader = "\<Space>"
nnoremap <Space> <Nop>

" emmet 
let g:user_emmet_leader_key='<C-z>'

"-- Goyo settings --
let g:goyo_width = 170
let g:goyo_height = 120
let g:goyo_margin_top = 1
let g:goyo_margin_bottom = 1

"-- bookmark settings --
let g:bookmark_save_per_working_dir = 0
let g:bookmark_auto_save = 1
let g:bookmark_display_annotation = 1

"-- airline settings --
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline_theme='onehalfdark'

let g:vimwiki_list = [{'path': '~/Documents/vimwiki/','syntax': 'markdown', 'ext': '.md'}]
"-- gutentag settings --
"let g:gutentags_cache_dir="~/.tagcache"
"let g:gutentags_ctags_exclude = ['build', 'static', 'node_modules']

" Ultisnips settings -
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-n>"
let g:UltiSnipsJumpBackwardTrigger="<c-p>"
let g:UltiSnipsSnippetDirectories=['~/.Ultisnips/']
let g:UltiSnipsEditSplit="vertical"

function! g:Open_browser(url)
	silent exe '!google-chrome-stable --app=' . a:url
endfunction
let g:mkdp_browserfunc = 'g:Open_browser'
let g:mkdp_markdown_css = '/home/anoop/.gfmarkdown.css'
" - vim session settings -
let g:session_autosave_periodic=10
let g:session_autosave = 'no'
let g:session_autoload = 'no'
" - disable perl provider 
let g:loaded_perl_provider = 0

lua require('anoopar')

"******************************** Key Mappings *******************************************
noremap     <F3> :Autoformat<CR>
noremap		<F4> :UltiSnipsEdit<CR>
imap        <F5> <C-R>=strftime("[%Y-%m-%d %a %I:%M %p]")<CR>
imap        <F6> <C-R>=strftime("%^b%d%^a%Y")<CR>
map         <F7> :set list! number! mouse=<CR>
nnoremap    <F8> :call Fiximports()<CR>
map         <F9> :AsyncRun octodown --style=github -l %<CR>
map         <F10> :ImportName<CR>
nnoremap    <F12> :TsuImport<CR>
imap		!! <C-R>=strftime("%Y%m%d_task")<CR>

nnoremap    <C-G> :Ggr <cword><CR>

nnoremap <silent> # :let save_cursor=getcurpos()\|let @/ = '\<'.expand('<cword>').'\>'\|set hlsearch<CR>w?<CR>:%s///gn<CR>:call setpos('.', save_cursor)<CR>
nnoremap <silent> * :let save_cursor=getcurpos()\|let @/ = '\<'.expand('<cword>').'\>'\|set hlsearch<CR>:%s///gn<CR>:call setpos('.', save_cursor)<CR>

nnoremap    <Tab><Right> :vertical resize -5<CR>
nnoremap    <Tab><Left> :vertical resize +5<CR>
nnoremap    <Tab><Up> :resize +5<CR>
nnoremap    <Tab><Down> :resize -5<CR>

map         gn :bn<cr>
map         gp :bp<cr>


cnoremap	sudow w !sudo tee % >/dev/null
nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> gD <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> gr <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> gi <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> K <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> <C-n> <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <silent> <C-p> <cmd>lua vim.lsp.diagnostic.goto_next()<CR>
inoremap <expr> <CR> pumvisible() ? "\<C-Y>" : "\<CR>"
inoremap jj <Esc>:w<cr>

nnoremap <leader>wj <C-W><C-J>
nnoremap <leader>wk <C-W><C-K>
nnoremap <leader>wl <C-W><C-L>
nnoremap <leader>wh <C-W><C-H>
nnoremap <leader>wc :q<cr>
nnoremap <leader>ww :vs \| :VimwikiIndex<CR>

nnoremap <leader>ff :NvimTreeFindFileToggle<CR>
nnoremap <leader>fr :lua vim.lsp.buf.format()<CR>

nnoremap <leader>jj <Esc>:w<cr>
nnoremap <leader>cn :noh<cr>

nnoremap <leader>tt :ToggleTask<CR>
nnoremap <leader>tc :CancelTask<CR>
nnoremap <leader>td :UndoTask<CR>

nnoremap <leader>pp <cmd>Telescope find_files<cr>
nnoremap <leader>ll <cmd>Telescope live_grep<cr>

nnoremap <leader>bb <cmd>Telescope buffers<cr>
nnoremap <leader>bd :bd<cr>
"****************************** Key Mappings End ******************************************
" -- custom functions --
command -nargs=+ Ggr execute 'silent Ggrep!' <q-args> | cw | redraw!

"nnoremap <C-!> <cmd>Telescope help_tags<cr>
" -- nvim lsp --
autocmd BufWritePre *.js lua vim.lsp.buf.format(nil, 100)
autocmd BufWritePre *.jsx lua vim.lsp.buf.format(nil, 100)
autocmd BufWritePre *.tsx lua vim.lsp.buf.format(nil, 100)
autocmd BufWritePre *.scss lua vim.lsp.buf.format(nil, 100)
autocmd BufWritePre *.py lua vim.lsp.buf.format(nil, 100)
let g:vimtex_compiler_progname = 'nvr'
let g:tex_flavor = 'latex'
