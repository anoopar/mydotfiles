[colors]

;background = #2f343f
background = #282828
background-alt = #4f545f
foreground = #d8dee8
foreground-alt = #787e68
accent = #81a1c1
alert = #ebcb8b
violet = #673AB7
indigo = #3F51B5
blue = #2196F3
green = #4CAF50
yellow = #FFEB3B
orange = #FF9800
red = #F44336
white = #FFFFFF
black = #000000
amber=#FFCA28
teal=#26A69A
cyan=#26C6DA
lightblue=#29B6F6
grey=#CFD8DC
pink=#FF4081

[bar/top]
width = 100%
height = 20
bottom = true
monitor = eDP1
monitor-strict = false
fixed-center = true
enable-ipc=true

;foreground = #eee
background = ${colors.background}
foreground = ${colors.white}

;underline-size = 0
;underline-color = #eee

padding-left = 0
padding-right = 0
padding-top = 0
padding-bottom = 0
padding=0
spacing=1

module-margin-left = 0 
module-margin-right = 0
module-margin=0
border-size =0

font-0 = Roboto Mono for Powerline:pixelsize=9;2
font-1 = Siji:style=Regular:size=9;1
font-2 = FontAwesome:pixelsize=7;2
font-3 = Material Design Icons:style=Regular:size=10;2
font-4 = FontAwesome:pixelsize=25;5

modules-left = bspwm mpd player-mpris-tail
modules-center =
modules-right =  tab1 tab2 a1 backlight a2 battery a1 temperature a2 wireless-network a1  volume a2 date

tray-position = right
;tray-padding = 2
;tray-maxsize = 15
;tray-scale=0.9

wm-restack = bspwm

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev

; MODULES

[module/bspwm]
type = internal/bspwm

ws-icon-0 = web;
ws-icon-1 = term;
ws-icon-2 = chat;
ws-icon-3 = code;
ws-icon-4 = music;
ws-icon-5 = slack;
ws-icon-6 = mail;
ws-icon-7 = ssh;
ws-icon-default = 

format = <label-state><label-mode>

label-focused = %icon%
label-focused-background = ${colors.white}
label-focused-foreground = ${colors.background}
label-focused-padding = 2

label-occupied = %icon%
label-occupied-foreground = ${colors.blue}
label-occupied-padding = 2

label-urgent = %icon%
label-urgent-foreground = ${colors.red}
label-urgent-padding = 2

label-empty = %icon%
label-empty-foreground = ${colors.grey}
label-empty-padding = 2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[module/mpd]
type = internal/mpd
format-online = <icon-prev> <toggle> <icon-next> <label-time>  <label-song>  
format-online-padding=1
icon-prev	= %{T4}%{T-}
icon-next	= %{T4}%{T-}
icon-play	= %{T4}%{T-}
icon-pause	= %{T4}%{T-}

icon-pause-foreground = ${colors.red}
label-song-maxlen = 45
label-song-ellipsis = true
;-------------------------------------------------------------------------------------
;					ARROWS
;-------------------------------------------------------------------------------------
[module/a1]
type = custom/text
content=%{T5}%{T-}
content-background=${colors.background}
content-foreground=${colors.white}
content-padding=0
content-margin=0
content-spacing=0

[module/a2]
type = custom/text
content=%{T5}%{T-}
content-background=${colors.white}
content-foreground=${colors.background}
content-padding=0
content-margin=0
content-spacing=0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[module/backlight]
type = internal/backlight
card = intel_backlight
enable-scroll = true
format = <ramp><label>
format-padding = 0
label = " %percentage%"
format-background=${colors.white}
format-foreground=${colors.background}
format-margin=0

ramp-0 = %{T4}%{T-}
ramp-1 = %{T4}%{T-}
ramp-2 = %{T4}%{T-}
ramp-3 = %{T4}%{T-}
ramp-4 = %{T4}%{T-}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[module/battery]
type = internal/battery
full-at = 99
battery = BAT0
adapter = AC0
poll-interval = 5
format-charging = <animation-charging><label-charging>
format-discharging = <ramp-capacity><label-discharging>
format-full = <ramp-capacity><label-full>
label-charging = " %percentage% "
label-discharging = " %percentage% "
label-full = " %percentage% "

format-charging-background=${colors.background}
format-discharging-background=${colors.background}
format-full-background=${colors.background}

label-charging-foreground = ${colors.green}
label-discharging-foreground = ${colors.white}
label-full-foreground = ${colors.white}

format-charging-foreground = ${colors.green}
format-discharging-foreground = ${colors.white}
format-full-foreground = ${colors.white}

format-charging-padding = 0
format-discharging-padding = 0
format-full-padding = 0

ramp-capacity-0 = %{T4}%{T-}
ramp-capacity-1 = %{T4}%{T-}
ramp-capacity-2 = %{T4}%{T-}
ramp-capacity-3 = %{T4}%{T-}
ramp-capacity-4 = %{T4}%{T-}
ramp-capacity-5 = %{T4}%{T-}
ramp-capacity-6 = %{T4}%{T-}
ramp-capacity-7 = %{T4}%{T-}
ramp-capacity-8 = %{T4}%{T-}
ramp-capacity-9 = %{T4}%{T-}

animation-charging-0 = %{T4}%{T-}
animation-charging-1 = %{T4}%{T-}
animation-charging-2 = %{T4}%{T-}
animation-charging-3 = %{T4}%{T-}
animation-charging-4 = %{T4}%{T-}
animation-charging-5 = %{T4}%{T-}
animation-charging-6 = %{T4}%{T-}
animation-charging-7 = %{T4}%{T-}
animation-charging-framerate = 750

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format =  <ramp><label> 
format-warn = <ramp><label-warn>
format-background = ${colors.white}
format-foreground = ${colors.background}
format-warn-background = ${colors.white}
format-warn-foreground = ${colors.red}

label = %temperature-c%
label-foreground = ${colors.background}
label-warn = %temperature-c%
label-warn-foreground = ${colors.red}

ramp-0 = %{T5}%{T-}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[module/wired-network]
type = internal/network
interface = enp1s0

format-connected = <ramp-signal><label-connected>
format-disconnected = <label-disconnected>
format-packetloss = <label-packetloss>

format-connected-padding= 1
format-disconnected-padding= 1
format-packetloss-padding= 1

format-connected-background = ${colors.green}
format-disconnected-background = ${colors.green}
format-packetloss-background = ${colors.green}

format-connected-foreground = ${colors.background}
format-disconnected-foreground = ${colors.background}
format-packetloss-foreground = ${colors.background}

label-connected = " %downspeed%  %upspeed%"
label-disconnected = %{T4}%{T-}
label-packetloss = %{T4}%{T-}
ramp-signal-0 = %{T4}%{T-} 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[module/wireless-network]
type = internal/network
interface = wlp2s0
interval = 15
format-connected = <ramp-signal><label-connected>
label-connected = " %downspeed%  %upspeed%"
label-connected-foreground = ${colors.white}
format-disconnected = <label-disconnected>
label-disconnected = %{T4} %{T-}
label-disconnected-foreground = ${colors.foreground-alt}

format-connected-background = ${colors.background}
format-connected-foreground = ${colors.white}
format-disconnected-background = ${colors.background}
ramp-signal-0 = %{T4}  %{T-}
ramp-signal-foreground = ${colors.white}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[module/volume]
master-mixer = Master
type = internal/alsa
format-volume = <ramp-volume><label-volume>
format-muted-prefix = %{T4}%{T-}
format-muted-foreground = ${colors.red}
format-volume-background=${colors.white}
format-volume-foreground=${colors.background}
format-muted-background=${colors.white}

ramp-volume-0 = %{T4}%{T-}
ramp-volume-1 = %{T4}%{T-}
ramp-volume-2 = %{T4}%{T-}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[module/date]
type = internal/date
interval = 1.0
date = %a %b %d, 
time = %I:%M:%S %p
date-alt = %A, %d %B %Y
time-alt = %a %b %d %H:%M:%S

label = "%time%"
format = <label>
format-padding = 1
format-background=${colors.background}
label-foreground =${colors.white}

[module/tab1]
type = custom/text
content = %{T4}%{T-}
click-left = /home/anoop/Apps/bin/starttab
content-padding = 1

[module/tab2]
type = custom/text
content = %{T4}%{T-}
click-left = /home/anoop/Apps/bin/starttab1
content-padding = 1

[module/player-mpris-tail]
type = custom/script
exec = ~/polybar-scripts/player-mpris-tail.py -f '{icon} {artist} - {title}' --icon-paused '' --icon-playing '' 
tail = true
label = %output%
click-left = ~/polybar-scripts/player-mpris-tail.py previous
click-right = ~/polybar-scripts/player-mpris-tail.py next
click-middle = ~/polybar-scripts/player-mpris-tail.py play-pause
label-padding = 2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ------------------------------------------------------- Second Bar -------------------------------------
[bar/top2]
width = 100%
height = 22
offset-x = 0
offset-y = 0 
monitor = HDMI1
monitor-strict = false
bottom = true

background = #262626
foreground = #eee

;underline-size = 2
;underline-color = #eee

spacing = 1
padding-left = 0
padding-right = 2
padding-top = 8

module-margin-left = 1 
module-margin-right = 1

font-0 = Roboto Mono for Powerline:pixelsize=9;3
font-1 = FontAwesome:size=7;3
font-2 = siji:pixelsize=7;3

modules-left = bspwm xwindow
modules-center = 
modules-right = date


wm-restack = bspwm

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev

