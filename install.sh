#!/bin/bash

# script to install all the files
if [ -d $HOME/.dotfiles ];then
	echo "No dotfiles directory.."
else
	mkdir $HOME/.dotfiles
	echo "Dotfiles directory created"
fi

cd $HOME/.dotfiles/ &&
git clone git@gitlab.com:anooparssss/mydotfiles.git .

if [ ! -d $HOME/.config/bspwm	];then
	mkdir $HOME/.config/bspwm &&
	ln -sf $HOME/.dofiles/bspwmrc $HOME/.config/bspwm/bspwmrc
else
	ln -sf $HOME/.dofiles/bspwmrc $HOME/.config/bspwm/bspwmrc
fi

if [ ! -d $HOME/.config/sxhkd	];then
	mkdir $HOME/.config/sxhkd &&
	ln -sf $HOME/.dofiles/sxhkdrc $HOME/.config/sxhkd/sxhkdrc
else
	ln -sf $HOME/.dofiles/sxhkdrc $HOME/.config/sxhkd/sxhkdrc
fi

if [ ! -d $HOME/.config/polybar	];then
	mkdir $HOME/.config/polybar &&
	ln -sf $HOME/.dofiles/polybarconfig $HOME/.config/polybar/config
else
	ln -sf $HOME/.dofiles/polybarconfig $HOME/.config/polybar/config
fi

if [ ! -f $HOME/.vimrc ];then
	ln -sf $HOME/..dofiles/vimrc.txt $HOME/.vimrc
fi

if [ ! -f $HOME/.ssh/config ];then
	ln -sf $HOME/.dotfiles/sshconfig $HOME/.ssh/config
fi
