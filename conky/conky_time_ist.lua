--[[
#=================================================
# Author  : anoop A R
#=================================================
]]

dofile("/home/anoop/.conky/conkyrc_common.lua")

conky.config["alignment"] = "bottom_left"
conky.config["gap_x"] = 30
conky.config["gap_y"] = conky.config["gap_y"] + 140 + padding
conky.config["minimum_height"] = 105

conky.text = [[
${voffset 0}${offset -10}${font1} TIME IST ${hr 2}
${voffset 10}${font2}${tztime Asia/Kolkata %I:%M:%S %p}
${voffset -20}${font3}${tztime Asia/Kolkata %a %b %d %Y - %d/%m/%Y }
]]
