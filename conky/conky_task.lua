--[[
#=================================================
# Author  : anoop A R
#=================================================
]]

dofile("/home/anoop/.conky/conkyrc_common.lua")

conky.config["alignment"] = "top_right"
conky.config["gap_x"] = 30
conky.config["minimum_height"] = 475

conky.text = [[
${voffset 0}${offset 0}${font1}TASKS ${hr 2}
${execpi 600 ~/Documents/Scripts/jira_tasks.py }

]]
