--[[
#=================================================
# Author  : Zvonimir Kucis
# Edited by anoop A R
#=================================================
]]

dofile("/home/anoop/.conky/conkyrc_common.lua")

conky.config["alignment"] = "bottom_left"
conky.config["gap_y"] = conky.config["gap_y"] + 295 + padding
conky.config["minimum_height"] = 105

conky.text = [[
# WEATHER
${execi 300 ~/.conky/scripts/weather.sh}
${voffset -10}${color1}${font :Bold:size=14}${execi 100 cat ~/.cache/weather.json | jq -r '.name'}, ${execi 100 cat ~/.cache/weather.json | jq -r '.sys.country'} $alignr${execi 100 ~/.conky/scripts/weather-icon.sh JDWS-02 $(cat ~/.cache/weather.json | jq -r '.weather[0].icon')}${image ~/.cache/weather-icon.png -p 180,10 -s 60x60}
${voffset 0}${color1}${font :Light:size=9}Weather will be ${execi 100 cat ~/.cache/weather.json | jq -r '.weather[0].main'}
${voffset 0}${color1}${font :Light:size=9}Temp : $alignr${execi 100 ~/.conky/scripts/kelvin2celsius.sh $(cat ~/.cache/weather.json | jq '.main.temp')}°C,
${voffset 0}${color1}${font :Light:size=9}Humidity : $alignr${execi 100 (cat ~/.cache/weather.json | jq '.main.humidity')}%
${voffset 0}${color1}${font :Light:size=9}Wind speed : $alignr${execi 100 (cat ~/.cache/weather.json | jq '.wind.speed')} mph${font Feather:size=9}
]]
