--[[
#=================================================
# Author  : Zvonimir Kucis
# Edited by Anoop A R
#=================================================
]]

dofile("/home/anoop/.conky/conkyrc_common.lua")

conky.config["alignment"] = "top_right"
conky.config["gap_x"] = 30
conky.config["minimum_height"] = 300

conky.text = [[
#------------+
# INFO
#------------+
#${color1}${font :size=12:bold}INFO ${hr 2}${font}
${voffset 5}${color3}${execi 6000 lsb_release -d | grep 'Descr'|awk {'print $2 " " $3" " $4" " $5'}} ${color3} $kernel ${color3} $uptime
#------------+
#CPU
#------------+
${voffset 10}${color1}${font :size=12:bold}CPU ${hr 2}${font}
${voffset 5}${color1}Name : ${color3}${execi 6000 cat /proc/cpuinfo | grep 'model name' | sed -e 's/model name.*: //'| uniq | cut -c 1-16} ${color1}Freq : ${color3}${freq_g} GHz
${color1}Usage : ${color3}${cpu}% ${color1}CPU : ${color3}${execi 5 sensors | grep CPU: | cut -c 16-19}°C ${color1}HDD :  ${color3}${execi 5 sensors | grep "Composite" | awk '{print $2}'}
#------------+
#CPU CORES
#------------+
#${voffset 2}${color1}CPU CORES ${stippled_hr 3 3}

# ${voffset 2}${color3}${cpu cpu1}%${goto 55}${color2}${cpubar cpu0 13 }
#${goto 170}${color1} 2${goto 195}${color3}${cpu cpu2}%${goto 235}${color2}${cpubar cpu2 13, 60}
#${voffset 2}${color1} 3${goto 45}${color3}${cpu cpu3}%${goto 85}${color2}${cpubar cpu3 13, 60}${goto 170}${color1} 4${goto 195}${color3}${cpu cpu4}%${goto 235}${color2}${cpubar cpu4 13, 60}
#${voffset 2}${color1} 5${goto 45}${color3}${cpu cpu5}%${goto 85}${color2}${cpubar cpu5 13, 60}${goto 170}${color1} 6${goto 195}${color3}${cpu cpu6}%${goto 235}${color2}${cpubar cpu6 13, 60}
#${voffset 2}${color1} 7${goto 45}${color3}${cpu cpu7}%${goto 85}${color2}${cpubar cpu7 13, 60}${goto 170}${color1} 8${goto 195}${color3}${cpu cpu8}%${goto 235}${color2}${cpubar cpu8 13, 60}
#------------+
#TEMPS
#------------+
#${voffset 5}${color1}${font :size=12:bold}TEMPS ${hr 2}${font}
#------------+
# PROCESSES
#------------+
${voffset 5}${color1}${font :size=12:bold}PROCESSES ${hr 2}${font}
${voffset 5}${color1}Name${goto 180}CPU%${alignr}MEM%
${color2}${top name 1} ${goto 180}${top cpu 1}${alignr}${top mem 1}${color3}
#${top name 2} ${goto 180}${top cpu 2}${alignr}${top mem 2}
#${top name 3} ${goto 180}${top cpu 3}${alignr}${top mem 3}
#${top name 4} ${goto 180}${top cpu 4}${alignr}${top mem 4}
#${top name 5} ${goto 180}${top cpu 5}${alignr}${top mem 5}
#------------+
# MEMORY
#------------+
${voffset 10}${color1}${font :size=12:bold}MEMORY ${hr 2}${font}
${voffset 5}${color1}Used: ${color3}$mem ($memperc%)${color1}${alignr}Free: ${color3}$memeasyfree
${color2}${membar}
${voffset 5}${color1}Name${goto 180}MEM%${alignr}MEM
${color2}${top_mem name 1} ${goto 180}${top_mem mem 1}${alignr}${top_mem mem_res 1}${color3}
#${top_mem name 2} ${goto 180}${top_mem mem 2}${alignr}${top_mem mem_res 2}
#${top_mem name 3} ${goto 180}${top_mem mem 3}${alignr}${top_mem mem_res 3}
#${top_mem name 4} ${goto 180}${top_mem mem 4}${alignr}${top_mem mem_res 4}
#${top_mem name 5} ${goto 180}${top_mem mem 5}${alignr}${top_mem mem_res 5}
#------------+
# GPU
#------------+
#${voffset 10}${color1}${font :size=12:bold}VIDEO ${hr 2}${font}
#${voffset 5}${color1}GPU :$alignr${color3}${execi 6000 nvidia-smi --query-gpu=gpu_name --format=csv,noheader,nounits}
#${color1}Driver :$alignr${color3}${execi 6000 nvidia-smi --query-gpu=driver_version --format=csv,noheader,nounits}
#${color1}Utilization :$alignr${color3}${exec nvidia-smi -i 0 | grep % | cut -c 61-63} %
#${color1}VRAM Utilization :$alignr${color3}${exec nvidia-smi -i 0| grep % | cut -c 37-40} MB
#------------+
# DISK
#------------+
#${voffset 10}${color1}${font :size=12:bold}DISK ${hr 2}${font}
# NVME
#${voffset 5}${color1}NVME ${stippled_hr 3 3}
#${voffset 5}${color1}Used: ${color3}${fs_used /}${color1}${goto 200}Free:${goto 250}${color3}${fs_free /}
#${color2}${fs_bar /}
#${voffset 2}${color1}Read: ${color3}${diskio_read nvme0n1}${goto 200}${color1}Write:${goto 250}${color3}${diskio_write nvme0n1}
#${color2}${diskiograph_read nvme0n1 40,130} ${alignr}${diskiograph_write nvme0n1 40,130}

# HDD
${voffset 0}${color1}HDD ${stippled_hr 3 3}
${voffset 5}${color1}Used: ${color3}${fs_used /}${color1}${goto 200}Free:${goto 250}${color3}${fs_free /}
${color2}${fs_bar /}
${voffset 2}${color1}Read: ${color3}${diskio_read /dev/nvme0n1p3}${goto 200}${color1}Write:${goto 250}${color3}${diskio_write /dev/nvme0n1p3}
#${color2}${diskiograph /dev/nvme0n1p3 25,130} 
#------------+
# NETWORK
#------------+
${voffset 5}${color1}${font :size=12:bold}NETWORK ${hr 2}${font}
${voffset 5}${color3}Up: ${upspeedf wlp3s0} KiB/s${alignr}Down: ${downspeedf wlp3s0} KiB/s
#${color2}${upspeedgraph wlp3s0 40,130 -l}$alignr${downspeedgraph wlp3s0 40, 130 -l}
#------------+
]]
