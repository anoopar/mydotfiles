--[[
#=================================================
# Author  : anoop A R
#=================================================
]]

dofile("/home/anoop/.conky/conkyrc_common.lua")

conky.config["alignment"] = "top_right"
conky.config["minimum_height"] = 475
conky.config["minimum_width"] = 410
conky.config["gap_x"] = 565

conky.text = [[
${voffset 0}${offset 0}${font1}AGENDA ${hr 2}
${execpi 6  ~/.conky/parseAgenda }

]]
