--[[
#=================================================
# Author  : anoop A R
#=================================================
]]

dofile("/home/anoop/.conky/conkyrc_common.lua")

conky.config["alignment"] = "bottom_left"
conky.config["minimum_height"] = 105

conky.text = [[
${voffset 0}${offset -10}${font1} TIME CET ${hr 2}
${voffset 10}${font2}${tztime Europe/Berlin %I:%M:%S %p}
${voffset -20}${font3}${tztime Europe/Berlin %a %b %d %Y - %d/%m/%Y }
]]
