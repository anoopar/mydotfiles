require("which-key").setup({})
-- require("todo-comments").setup({})

-- vimwiki configuaration
vim.g.vimwiki_list = {
	{ path = "~/Documents/vimwiki/", syntax = "markdown", ext = ".md" },
}

vim.g.vimwiki_global_ext = 0
-- vim bookmarks configuration
vim.g.bookmark_save_per_working_dir = 0
vim.g.bookmark_auto_save = 1
vim.g.bookmark_display_annotation = 1
